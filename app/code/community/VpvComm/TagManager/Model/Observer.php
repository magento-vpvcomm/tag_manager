<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 28.03.17
 * Time: 10:07
 */
class VpvComm_TagManager_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function getBannerCollection(Varien_Event_Observer $observer)
    {
        try {
            $collection = $observer->getEvent()->collection;
            if (is_object($collection) && count($collection->getItems())>0) {
                VpvComm_TagManager_Model_AggregatorBanners::processMirasvitBanners($collection);
            } else if (is_array($collection) && count($collection) > 0) {
                VpvComm_TagManager_Model_AggregatorBanners::processMagentoBanners($collection);
            }
        } catch (Exception $e) {
            Mage::helper('vpvcomm_tagmanager')->logLog('gtm.observerBanners','Observer Error Banners (aristos_banners_collection_load_after)',$e);
        }
    }

    /**
     * сбор коллекций из виджетов выводящих товары
     * класс виджетов = Aristos_Widgets_Block_Widget_Special
     * @param Varien_Event_Observer $observer
     */
    public function getWidgetCollection(Varien_Event_Observer $observer)
    {
        try {
            $collection = $observer->getEvent()->collection;
            $class = get_class($collection);
            $arrGetItems = [
                'Mage_Catalog_Model_Resource_Product_Collection',
                'Mage_Catalog_Model_Resource_Product_Link_Product_Collection',
            ];
            if (in_array($class,$arrGetItems)) {
                $collection = $collection->getItems();
            }
            VpvComm_TagManager_Model_AggregatorProducts::initArrayFromWidget($collection);
        } catch (Exception $e) {
            Mage::helper('vpvcomm_tagmanager')->logLog('gtm.observerBanners','Observer Error Products (aristos_widgets_collection_load_after)',$e);
        }
    }
}

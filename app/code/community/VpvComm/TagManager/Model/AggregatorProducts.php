<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 23.03.17
 * Time: 13:58
 */
class VpvComm_TagManager_Model_AggregatorProducts
{
    // первые 18 товаров
    public static $arrProdsBefore = [];
    // товары от 19 и выше
    public static $arrProdsAfter = [];
    // товары из виджетов
    public static $arrFromWidget = [];
    // сбор продуктов из рекурсии
    protected static $storeProds = [];

    /**
     * инициализация $arrFromWidget данными из виджетов
     * проверка URI больше не производится
     * @param $collection = $this->getLoadedProductCollection()
     * @param $mageApp = Mage::app()->getRequest()
     */
    public static function initArrayFromWidget($collection)
    {
        self::$arrFromWidget[] = $collection;
    }

    /**
     * подготовка товаров из коллекций для jsonProducts
     * @param $collection
     * @return mixed
     */
    protected static function prepareFromWidget($collection)
    {
        array_walk_recursive($collection, function($item){
            if (get_class($item) == 'Aristos_Erp_Model_Catalog_Product') {
                array_push(self::$storeProds,$item);
            }
        });
        $result = Mage::helper('vpvcomm_tagmanager')->iterateImpressions(self::$storeProds);
        return $result;
    }

    /**
     * подготовка json-строки
     * @return string
     */
    public static function getJsonProducts($sellArray = false)
    {
        $json = true;
        $i = 1;
        try {
            $arrResult = [];
            $arrClear = [];
            $arrWidget = [];
            $after = current(self::$arrProdsAfter);
            if ($after) {
                foreach ($after as $val) {
                    foreach ($val as $point) {
                        array_push($arrClear,$point);
                    }
                }
            }
            if (count(self::$arrFromWidget)>0) {
                $arrWidget = self::prepareFromWidget(self::$arrFromWidget);
            }
            $arrClear = array_merge($arrClear,$arrWidget);

            /**
             * если есть массив-аргумент из товаров sell
             * upsell, crossell
             */
            if ($sellArray) {
                $arrClear = self::prepareFromWidget($sellArray);
            }
            // добавляем ключи для поиска по ним
            foreach ($arrClear as $item) {
                $sku = $item['id'];
                $arrResult[$sku] = $item;
            }
            // перебор на случай задвоения как на Grohe
            foreach ($arrResult as $key => $val) {
                $val['position'] = $i;
                $i++;
                $arrResult[$key] = $val;
            }
            $json = json_encode($arrResult, JSON_UNESCAPED_UNICODE); // JSON_PRETTY_PRINT
            $json = Mage::helper('vpvcomm_tagmanager')->clearingJson($json);
        } catch (Exception $e) {
            Mage::helper('vpvcomm_tagmanager')->logLog('gtm.jsonProducts','Error jsonProducts',$e);
        }
        return $json;
    }

    /**
     * массив-коллекция upsell-товаров
     * @return array
     */
    public static function getUpsellItems()
    {
        $upsell = [];
        /** @var Enterprise_TargetRule_Block_Catalog_Product_List_Upsell $block */
        if ($block = Mage::app()->getLayout()->getBlock('product.info.upsell')) {
            $items = $block->getItemCollection();
            if (is_array($items)) {
                foreach($items as $item) {
                    $upsell[] = $item;
                }
            }
        }
        return $upsell;
    }

    /**
     * массив-коллекция related-товаров
     * @return array
     */
    public static function getRelatedItems()
    {
        $related = [];
        /** @var Enterprise_TargetRule_Block_Catalog_Product_List_Related $block */
        if ($block = Mage::app()->getLayout()->getBlock('catalog.product.related')) {
            $related = $block->getItemCollection();
            if (empty($related)) $related = [];
        }
        return $related;
    }

    /**
     * общий массив-коллекция для upsell-товаров и related-товаров
     * @return array
     */
    public static function getRelatedProducts()
    {
        $upsell = self::getUpsellItems();
        $related = self::getRelatedItems();
        return array_merge($upsell, $related);
    }

    /**
     * @return bool|string
     */
    public static function getJsonSellProducts()
    {
        $productsJson = false;
        if (Mage::registry('current_product')) {
            $products = self::getRelatedProducts();
            $productsJson = self::getJsonProducts($products);
        }
        return $productsJson;
    }
}

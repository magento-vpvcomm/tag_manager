<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 31.03.17
 * Time: 11:42
 */
class VpvComm_TagManager_Model_AggregatorBanners
{
    use Aristos_Base_Trait_LazyCache;
    public static $arrFromWidget = [];
    const TYPE_MAGENTO = 'MAGE';
    const TYPE_MIRASVIT = 'MIRA';
    const CREATIVE_SLIDE =   'slideshow';
    const CREATIVE_STATIC  =   'static';

    /**
     * Процессинг коллекции баннеров Mirasvit
     * @param Mirasvit_Banner_Model_Resource_Banner_Collection $collection
     */
    public static function processMirasvitBanners($collection)
    {
        /** @var Mirasvit_Banner_Model_Banner $banner */
        foreach ($collection as $banner) {
            $creative = self::CREATIVE_STATIC;
            if ($banner->getAdType() == 'slideshow')
                $creative = self::CREATIVE_SLIDE;
            self::addBanner($banner->getId(), $banner->getName(), $banner->getImage(), $creative, self::TYPE_MIRASVIT);
        }
    }

    /**
     * Процессинг массива с баннерами Magento
     * @param array $banners
     */
    public static function processMagentoBanners(array $banners)
    {
        foreach ($banners as $id => $content) {
            if (preg_match('/src="(.*?)"/', $content, $source)) {
                preg_match('/href="(.*?)"/', $content, $url);
                $link = $url[1] ?: 'Banner';
                self::addBanner($id, $link, $source[1]);
            }
        }
    }

    /**
     * Добавить информацию о баннере в стек
     * https://developers.google.com/tag-manager/enhanced-ecommerce
     * @param $id
     * Идентификатор баннера
     * @param $name
     * Название промо
     * @param $src
     * Файл с изображением
     * @param string $creative
     * Вид/Признак баннера
     * @param string $type
     * Тип баннера, self::TYPE_MAGENTO или self::TYPE_MIRASVIT
     */
    public static function addBanner($id, $name, $src, $creative = self::CREATIVE_STATIC, $type = self::TYPE_MAGENTO)
    {
        $id = $id . "_" . $type;
        $banner = [
            'id' => $id,
            'name' => $name,
            'creative' => $creative,
            'position' => count(self::$arrFromWidget) + 1,
            'src' => $src,
        ];
        self::$arrFromWidget[] = $banner;
    }

    /**
     * подготовка json-строки
     * @return string
     */
    public static function getJsonBanners()
    {
        $json = true;
        try {
            $json = json_encode(self::$arrFromWidget, JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            Mage::helper('vpvcomm_tagmanager')->logLog('gtm.jsonBanners','Error jsonBanners',$e);
        }
        return $json;
    }
}

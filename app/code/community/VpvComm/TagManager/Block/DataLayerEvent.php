<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 10.11.16
 * Time: 13:49
 */

/**
 * формирование dataLayer ПОСЛЕ контейнера
 * наполнение происходит по событию
 * объект dataLayer формируется с использовнием 'push'
 */
class VpvComm_TagManager_Block_DataLayerEvent extends VpvComm_TagManager_Block_DataLayer
{

    /**
     * формирование jsonPage
     * строки в формате json, которая выводит ее в html-код
     * эти данные необходимы для обработки в javascript
     * @return string
     */
    public function getJsonPage()
    {
        try {
            $helper = Mage::helper('vpvcomm_tagmanager');
            $arr = [];
            $arr['pageCategory'] = $this->pageCategory;
            $arr['seenProducts'] = $helper->getSeenProducts();
            $arr['formFields']   = $helper->formFields();
            if ($this->pageCategory === 'Checkout') {
                $arr['allBasket'] = $helper->allBasket($this->pageCategory);            }
            elseif ($this->pageCategory === 'ProductPage') {
                $arr['productAddBasket'] = $helper->productAddBasket($this->pageCategory);
                $arr['tabSwitch'] = $helper->tabSwitch();
            }
            elseif (in_array($this->pageCategory,$this->categoryGroup)) {
                $arr['pageCategory'] = 'CategoryGroup';
                $arr['productClick'] = $helper->productClick();
                $arr['productAddCompare'] = $helper->productAddCompare();
            }
        } catch (Exception $e) {
            Mage::helper('vpvcomm_tagmanager')->error('Ошибка формирования DataLayer: %s', $e->__toString());
            $arr = [];
        }
        // удаление апострофа из json-строки
        $json = json_encode($arr, JSON_UNESCAPED_UNICODE); // JSON_PRETTY_PRINT;
        $json = Mage::helper('vpvcomm_tagmanager')->clearingJson($json);
        return $json;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->isEnableGTM())
            return '';
        return parent::_toHtml();
    }

    /**
     * группа категорий страниц
     * с единым набором событий
     * @var array
     */
    protected $categoryGroup = [
        'CatalogProductGroup',
        'CatalogCategoryGroup',
        'SalePortal',
        'SaleCatalog',
        'MarkdownCatalog',
        'ComparePage',
        'PageViewTotal',
        'CatalogTotal',
    ];
}

<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 10.11.16
 * Time: 13:43
 */

/**
 * родительский класс семейства классов
 * для работы с метриками Google
 * Class VpvComm_TagManager_Block_DataLayer
 */
class VpvComm_TagManager_Block_DataLayer extends Mage_Core_Block_Template
{
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->isEnableGTM())
            return '';
        return parent::_toHtml();
    }

    /** @var Aristos_Base_Model_Customer_Session  */
    protected $session = false;
    /** @var Aristos_Customer_Model_Customer_Customer  */
    protected $user = false;
    // страница
    protected $page = false;
    // текущая категория страницы
    protected $pageCategory = false;
    // хранилище для настроек категорий страниц
    protected $config_category = false;
    // хранилище для настроек событий страниц
    protected $config_alias = false;
    // текущий URL
    protected $pageUrl = false;

    /**
     * инициализируем постоянно нужные поля
     * доступ к которым может понадобиться отовсюду
     * VpvComm_TagManager_Block_DataLayer constructor.
     */
    public function _construct()
    {
        $this->session = Mage::getSingleton('customer/session');
        $this->user = $this->session->getCustomer();
        $this->page = Mage::app()->getFrontController()->getAction()->getFullActionName();
        $this->pageUrl = Mage::app()->getRequest()->getRequestString();
        // сохранили настройки категорий страниц из админки
        $this->config_category = $this->getConfigCategory();
        // сохранили настройки алиасов страниц из админки
        $this->config_alias = $this->getConfigAlias();
        if (is_array($this->config_category)) {
            $this->config_category = array_merge($this->pageMethodRel,$this->config_category);
        } else {
            $this->config_category = $this->pageMethodRel;
        }
        if (array_key_exists($this->page,$this->config_category)) {
            $this->pageCategory = $this->config_category[$this->page];
        }
        return parent::_construct();
    }

    /**
     * работа с конфигурацией категорий страниц на текущем проекте
     * получение, проверка, запись в реестр
     * @return bool|mixed
     */
    protected function getConfigCategory()
    {
        $result = false;
        // поулчаем реестр
        $conf_cat = Mage::registry('config_category');
        if ( !empty($conf_cat) ) { // если реестр не пустой
            $result = $conf_cat;
        } else { // если реестр пустой
            // получаем настройки из админки
            $conf = Mage::getStoreConfig('vpvcomm_tagmanager/page_category/json');
            // декодируем для проверки
            $arr = json_decode($conf, true);
            if ( !empty($arr) ) {
                // вносим json-конфиг в реестр
                Mage::register('config_category', $conf);
                // выводим массив настроек
                $result = $arr;
            }
        }
        return $result;
    }

    /**
     * работа с конфигурацией алиасов страниц на текущем проекте
     * получение, проверка, запись в реестр
     * @return bool|mixed
     */
    protected function getConfigAlias()
    {
        $result = false;
        // поулчаем реестр
        $conf_al = Mage::registry('config_alias');
        if ( !empty($conf_al) ) { // если реестр не пустой
            $result = !is_array($conf_al) ? json_decode($conf_al, true) : $conf_al;
        } else { // если реестр пустой
            // получаем настройки из админки
            $conf = Mage::getStoreConfig('vpvcomm_tagmanager/page_alias/json');
            // декодируем для проверки
            $arr = json_decode($conf, true);
            if ( !empty($arr) ) {
                // вносим json-конфиг в реестр
                Mage::register('config_alias', $conf);
                // выводим массив настроек
                $result = $arr;
            }
        }
        return $result;
    }

    /**
     * идентификация пользователя
     * @return array
     */
    protected function checkUser()
    {
        $arr = [
            'userAuth' => '0',
        ];

        if ($this->session && $this->session->isLoggedIn()) {
            $customerGroupId = $this->session->getCustomerGroupId();
            if (!($groupName = $this->session->getData('group_name'))) {
                $groupName = Mage::getModel('customer/group')->load($customerGroupId)->getCustomerGroupCode();
                $groupName = empty($groupName) ? 'General' : $groupName;
                $this->session->setData('group_name', $groupName);
            }
            $arr['userAuth'] = '1';
            $arr['userId'] = $this->user->getId();
            $arr['userType'] = $groupName;
        }

        return $arr;
    }

    /**
     * связь между алиасом URL и соответствующим методом
     * необходимо из-за практики использования редиректов
     * @var array
     */
    protected $aliasMethodRel = [
        'CatalogProductGroup',
        '/hot-offers.html' => 'SalePortal',
        '/intraclean.html' => 'OverviewPage',
    ];

    /**
     * связь между текущим  URL и соответствующим ей методом
     * уникальный префикс метода указывается в дочерних классах
     * @var array
     */
    protected $pageMethodRel = [
        'cms_index_index' => 'Main',
        'cms_page_view'                 => 'PageViewTotal',
        'catalog_category_view'         => 'CatalogTotal',
        'catalog_product_view'          => 'ProductPage',
        'checkout_cart_index'           => 'Checkout',
        'checkout_onepage_index'        => 'Checkout',
        'checkout_onepage_success'      => 'ThankYouPage',
        'customer_account_index' => 'Profile',
        'kbase_article_index'           => 'InfoPortal',
        'kbase_article_category'        => 'InfoCategory',
        'kbase_article_article'         => 'InfoPage',
        'catalogsearch_result_index'    => 'SearchResults',
        'cms_index_noRoute'             => '404Page',
    ];

    /**
     * @return string Google Tag Id
     */
    public function getGoogleTagId()
    {
        return Mage::getStoreConfig('vpvcomm_tagmanager/general/google_tag_id');
    }

    /**
     * @return string Yandex Metrika Id
     */
    public function getYandexMetrikaId()
    {
        return Mage::getStoreConfig('vpvcomm_tagmanager/yandex_metrika/id');
    }

    /**
     * проверка: включен ли TagManager для данного сайта
     * @return mixed
     */
    public function isEnableGTM()
    {
        return Mage::getStoreConfig('vpvcomm_tagmanager/general/enable_gtm');
    }

    /**
     * @return string Yandex Metrika Counter Code
     */
    public function getYandexMetrikaCode()
    {
        return Mage::getStoreConfig('vpvcomm_tagmanager/yandex_metrika/code');
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 07.08.17
 * Time: 11:46
 */

/**
 * сбор данных Upsell и Related продуктов для dataLayer
 * Class VpvComm_TagManager_Block_SellProducts
 * ветка для Дмитрий Янкович  14 авг 2017, 14:03 выкатить текущую версию на shop.grundfos.ru
 */
class VpvComm_TagManager_Block_SellProducts extends Mage_Core_Block_Template
{

    /**
     * объект класса
     * @return Enterprise_TargetRule_Block_Catalog_Product_List_Upsell
     */
    public function objectUpsell()
    {
        return new Enterprise_TargetRule_Block_Catalog_Product_List_Upsell();
    }

    /**
     * массив-коллекция upsell-товаров
     * @return array
     */
    public function getUpItemCollection()
    {
        return $this->objectUpsell()->getItemCollection();
    }

    /**
     * объект класса
     * @return Enterprise_TargetRule_Block_Catalog_Product_List_Related
     */
    public function objectRelated()
    {
        return new Enterprise_TargetRule_Block_Catalog_Product_List_Related();
    }

    /**
     * массив-коллекция related-товаров
     * @return array
     */
    public function getRelatedItemCollection()
    {
        return $this->objectRelated()->getItemCollection();
    }

    /**
     * общий массив-коллекция для upsell-товаров и related-товаров
     * @return array
     */
    public function getItemCollection()
    {
        $collectionUp = $this->getUpItemCollection();
        $collectionRelated = $this->getRelatedItemCollection();
        $up = $collectionUp ? $collectionUp : [];
        $related = $collectionRelated ? $collectionRelated : [];
        return array_merge($up, $related);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 10.11.16
 * Time: 13:49
 */

/**
 * формирование dataLayer ДО контейнера по категориям страниц
 * название метода соответствует названию категории страницы без префикса 'pc'
 * объект dataLayer формируется без использовния 'push'
 */
class VpvComm_TagManager_Block_DataLayerPage extends VpvComm_TagManager_Block_DataLayer
{

    protected $ecommerce = false;
    // уникальный префикс методов класса
    protected $prefix = 'pc';

    /**
     * формирование объекта datalayer до контейнера Google Tag Manager
     * на основе вызова метода класса через переменную
     * @return string
     */
    public function getDataLayerPage()
    {
        // блок по-умолчанию
        $block = "\n<!-- dataLayerBefore is empty -->\n";
        $checkType = is_array($this->config_category) ? array_key_exists($this->page,$this->config_category) : false;
        $checkAlias = is_array($this->config_alias) ? array_key_exists($this->pageUrl,$this->config_alias) : false;
        try {
            // если страница зарегистрирована
            if ($checkType) {
                /**
                 * получаем имя нужного метода-обработчика
                 * смена метода-обработчика для отдельного URL
                 * алиас приоритетнее чем macUrl !
                 */
                $nameMethod = $checkAlias ? $this->config_alias[$this->pageUrl] : $this->config_category[$this->page];
                $nameMethod = $this->prefix . $nameMethod;
                // проверка наличия метода
                if (method_exists($this,$nameMethod)) {
                    // формируем красивый json из dataLayer из метода-переменной
                    $json = json_encode($this->$nameMethod(), JSON_UNESCAPED_UNICODE); // JSON_PRETTY_PRINT
                } else {
                    $json = json_encode($this->pcNoMethod($nameMethod), JSON_UNESCAPED_UNICODE);
                }
                // инициируем javascript-объект
                $data = "<script>
                    var dataLayer = window.dataLayer = dataLayer || [];
                    dataLayer.push({$json});
                </script>";
                // формируем блок для вывода на страницу
                $block = "\n<!-- dataLayerBeforeContainer -->\n{$data}\n<!-- End dataLayerBeforeContainer -->\n";
            }
        } catch(Exception $ex) {
            $currentUrl = Mage::helper('core/url')->getCurrentUrl();
            $text = "{$ex->getMessage()}\r\nUrl: {$currentUrl}";
            Mage::helper('vpvcomm_tagmanager')->error($text);
        }
        return $block;
    }

    /**
     * структура dataLayer
     * для типа страницы Main из ТЗ
     * @return array
     */
    public function pcMain()
    {
        $arr = [
            'pageType' => 'Main',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer для нескольких типов страниц из ТЗ
     * CatalogProductGroup, SaleCatalog, SalePortal, OverviewPage
     * @return array
     */
    public function pcPageViewTotal()
    {
        $anyType = ['SaleCatalog','CatalogProductGroup'];
        $pageSale = false;
        $pc_arr = [];

        $catalogName = $this->getLayout()->getBlock('head')->getTitle();
        if (strpos($catalogName,' - ')!==false) {
            $catalogName = trim(substr($catalogName,0,strpos($catalogName,' - ')));
        }
        // добавили свои правила из админки
        if ($this->config_alias) {
            $pc_arr = array_merge($this->aliasMethodRel,$this->config_alias);
        }
        if (key_exists($this->page,$this->config_category)) {
            $arr['pageType'] = $this->config_category[$this->page];
        } elseif (key_exists($this->pageUrl,$pc_arr)) {
            $arr['pageType'] = $pc_arr[$this->pageUrl];
        } else {
            $arr['pageType'] = $pageSale === false ? $anyType[1] : $pageSale;
            # $this->ecommerce['impressions'] = Mage::helper('vpvcomm_tagmanager')->getImpressions();
        }
        if (in_array($arr['pageType'],$anyType)) {
            // то что невозможно определить
            $arr['catalogName'] = $catalogName;
            $arr['catalogId'] = '1';
            $arr['catalogLevel'] = '1';
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для двух типов страниц Catalog и CatalogCategoryGroup из ТЗ
     * @return array
     */
    public function pcCatalogTotal()
    {
        $cat = Mage::registry('current_category');
        // уровень текущей страницы
        $level = Mage::helper('vpvcomm_tagmanager')->getRightLevel($cat);
        // тип страницы каталога
        $pageType = Mage::helper('vpvcomm_tagmanager')->getCatalogPageType($cat);
        $arr = [
            'pageType' => $pageType,
            'catalogId' => $cat->getId(),
            'catalogName' => $cat->getName(),
            'catalogLevel' => $level,
            'catalogPage' => Mage::helper('vpvcomm_tagmanager')->getCatalogPage(),
        ];
        // тип страницы для разных уровней
        $this->pageCategory = $pageType;
        $impress = Mage::helper('vpvcomm_tagmanager')->getImpressions('current_category',$this->pageCategory);
        if (count($impress) != 0) {
            $this->ecommerce['impressions'] = $impress;
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы ProductPage из ТЗ
     * @return array
     */
    public function pcProductPage()
    {
        $helper = Mage::helper('vpvcomm_tagmanager');
        $prod = Mage::registry('current_product');
        $pageType = 'ProductPage';
        $arr = [];
        if ($category = $prod->getCategory()) {
            $arr = [
                'catalogId' => $category->getId(),
                'catalogName' => $category->getName(),
                'catalogLevel' => $category->getLevel()
            ];
        }
        $arr['pageType'] = $pageType;
        $arr['productId'] = $prod->getSku();
        $arrImpress = [];
        $additionalProducts = $helper->getAdditionalProducts();
        if ($additionalProducts) {
            $arrImpress = array_merge($arrImpress,$additionalProducts);
        }
        if (Mage::getStoreConfig('vpvcomm_tagmanager/general/recently_viewed')) {
            $seenProducts= $helper->getSeenProducts();
            if ($seenProducts) {
                $arrImpress = array_merge($arrImpress,$seenProducts);
            }
        }
        if (count($arrImpress)>0) {
            $this->ecommerce['impressions'] = $arrImpress;
        }
        $products = $this->getProducts($pageType);
        $this->ecommerce['detail'] = [
            'list' => $pageType,
            'products' => [ $products ],
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы Checkout из ТЗ
     * @return array
     */
    public function pcCheckout()
    {        // сессия для корзины
        $checkoutSess = Mage::getSingleton('checkout/session');
        // товары в корзине
        $items= $checkoutSess->getQuote()->getAllVisibleItems();
        $arr = [
            'pageType' => 'Checkout',
        ];
        $this->ecommerce['checkout']['products'] = $this->getProductsCheckout($items);
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы ThankYouPage из ТЗ
     * @return arrayclear
     */
    public function pcThankYouPage()
    {
        // номер последнего заказа
        $lastOrder = Mage::getSingleton('checkout/session')->getLastOrderId();
        // данные заказа
        $order = Mage::getSingleton('sales/order')->load($lastOrder);
        // данные из сайта
        $affiliation  = Mage::getStoreConfig('general/store_information/name');
        // сумма заказа
        $revenue = Mage::helper('vpvcomm_tagmanager')->priceGood($order->getGrandTotal());
        // сумма доставки
        $shipping = Mage::helper('vpvcomm_tagmanager')->priceGood($order->getShippingAmount());
        // код промокода
        $couponCode = $order->getCouponCode();
        $coupon = $couponCode ? $couponCode : '';
        // номер заказа
        $orderId = $order->getIncrementId();
        // способ оплаты
        $paymentType = $order->getPayment()->getMethod();
        // метод доставки
        $shippingMethod = $order->getShippingMethod();
        // товары в заказе
        $items = $order->getAllVisibleItems();
        $products = $this->getProductsCheckout($items);
        $shpOrder = $order;
        $shpDeliveryTax = Mage::helper('vpvcomm_tagmanager')->priceGood($shpOrder->getShippingAmount());
        $arr = [
            'pageType' => 'ThankYouPage',
            'paymentType' => $paymentType,
            'shippingMethod' => $shippingMethod,
            'ecommerce' => [
                'purchase' => [
                    'actionField' => [
                        'id' => $orderId,                   # Id заказа
                        'affiliation' => $affiliation,      # название точки касания
                        'revenue' => $revenue,              # сумма заказа
                        'shipping' => $shpDeliveryTax,      # стоимость доставки
                        'coupon' => $coupon,                # код промокода
                    ],
                    'products' => $products,
                ],
            ]
        ];
        if (Mage::getStoreConfig('vpvcomm_tagmanager/general/recently_viewed')) {
            $lastSeen = Mage::helper('vpvcomm_tagmanager')->getSeenProducts();
            if ($lastSeen) {
                $arr['ecommerce']['impressions'] = $lastSeen;
            }
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы SearchResults из ТЗ
     * @return array
     */
    public function pcSearchResults()
    {
        $prods = Mage::getSingleton('catalogsearch/layer')->getProductCollection()->getItems();
        $arr = [
            'pageType' => 'SearchResults',
        ];
        $this->ecommerce['impressions'] = Mage::helper('vpvcomm_tagmanager')->getSearchImpressions($prods,$this->pageCategory);
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы 404Page из ТЗ
     * @return array
     */
    public function pc404Page()
    {
        $arr = [
            'pageType' => '404Page',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы Profile из ТЗ
     * @return array
     */
    public function pcProfile()
    {
        $arr = [
            'pageType' => 'Profile',
        ];
        if (Mage::getStoreConfig('vpvcomm_tagmanager/general/recently_viewed')) {
            $lastSeen = Mage::helper('vpvcomm_tagmanager')->getSeenProducts();
            if ($lastSeen) {
                $this->ecommerce['impressions'] = $lastSeen;
            }
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы SaleCatalog из ТЗ
     * @return array
     */
    public function pcSalePortal()
    {
        $title = $this->getLayout()->getBlock('head')->getTitle();
        $catalogName = trim(substr($title,0,strrpos($title,' - ')));
        $catalogId = Mage::getBlockSingleton('cms/page')->getPage()->getId();
        $arr = [
            'pageType' => 'SalePortal',
            'catalogId' => $catalogId,
            'catalogName' => $catalogName,
            'catalogLevel' => '1',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы SaleCatalog созданного вне ТЗ по необходимости
     * example: /lightsale.html
     * @return array
     */
    public function pcSaleCatalog()
    {
        $title = $this->getLayout()->getBlock('head')->getTitle();
        $catalogName = trim(substr($title,0,strrpos($title,' - ')));
        $catalogId = Mage::getBlockSingleton('cms/page')->getPage()->getId();
        $arr = [
            'pageType' => 'SaleCatalog',
            'catalogId' => $catalogId,
            'catalogName' => $catalogName,
            'catalogLevel' => '1',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы pcOverViewPortal из ТЗ
     * @return array
     */
    public function pcOverViewPortal()
    {
        $arr = [
            'pageType' => 'OverViewPortal',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы MarkdownCatalog из ТЗ
     * @return array
     */
    public function pcMarkdownCatalog()
    {
        $arr = [
            'pageType' => 'MarkdownCatalog',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы pcInfoPortal из ТЗ
     * @return array
     */
    public function pcInfoPortal()
    {
        $arr = [
            'pageType' => 'InfoPortal',
        ];
        if (Mage::getStoreConfig('vpvcomm_tagmanager/general/recently_viewed')) {
            $lastSeen = Mage::helper('vpvcomm_tagmanager')->getSeenProducts();
            if ($lastSeen) {
                $this->ecommerce['impressions'] = $lastSeen;
            }
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы pcInfoCategory из ТЗ
     * @return array
     */
    public function pcInfoCategory()
    {
        $arr = [
            'pageType' => 'InfoCategory',
        ];
        if (Mage::getStoreConfig('vpvcomm_tagmanager/general/recently_viewed')) {
            $lastSeen = Mage::helper('vpvcomm_tagmanager')->getSeenProducts();
            if ($lastSeen) {
                $this->ecommerce['impressions'] = $lastSeen;
            }
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы InfoPage из ТЗ
     * @return array
     */
    public function pcInfoPage()
    {
        $arr = [
            'pageType' => 'InfoPage',
        ];
        if (Mage::getStoreConfig('vpvcomm_tagmanager/general/recently_viewed')) {
            $lastSeen = Mage::helper('vpvcomm_tagmanager')->getSeenProducts();
            if ($lastSeen) {
                $this->ecommerce['impressions'] = $lastSeen;
            }
        }
        return $this->arrCombiner($arr);
    }

    /**
     * структура dataLayer
     * для типа страницы ComparePage из ТЗ
     * @return array
     */
    public function pcComparePage()
    {
        $arr = [
            'pageType' => 'ComparePage',
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * проверка необходимости вывода массива
     * по ключу ecommerce
     * @return array
     */
    protected function checkEcommerce()
    {
        $arr = [];
        if ($this->ecommerce) {
            $arr['ecommerce'] = $this->ecommerce;
        }
        return $arr;
    }

    /**
     * унификация объединения нескольких массивов
     * в результирующий dataLayer
     * @param $arr
     * @return array
     */
    protected function arrCombiner($arr)
    {
        return array_merge(
            $this->checkUser(),
            $arr,
            $this->checkEcommerce()
        );
    }

    /**
     * @param $items
     * @return array
     */
    protected function getProductsCheckout($items)
    {
        $arr = [];
        $layer = Mage::helper('vpvcomm_tagmanager');
        $notNeed = ['position','list', 'id_cat'];
        foreach ($items as $val) {
            $qty = $val->getQty();
            $quantity = is_null($qty) ? (int) $val->getQtyOrdered() : $qty;
            $arr[] = $layer->improdGenerate($val, $quantity, false, false, $notNeed);
        }
        return $arr;
    }

    /**
     * формирование массива products
     * @param bool $newArr
     * @return array|bool
     */
    protected function getProducts($list,$newArr=false)
    {
        /** @var Aristos_Erp_Model_Catalog_Product $product */
        $product = Mage::registry('current_product');
        $arr = Mage::helper('vpvcomm_tagmanager')->improdGenerate($product, 1, 1, $list, $notNeed = ['position']);
        if ($newArr) {
            $arr = $newArr;
        }
        return $arr;
    }

    /**
     * метод для отлова несуществующих
     * или неработающих типов страниц
     * @param $anyMethodName
     * @return array
     */
    public function pcNoMethod($anyMethodName)
    {
        $arr = [
            'pageType' => str_replace('pc','',$anyMethodName),
        ];
        return $this->arrCombiner($arr);
    }

    /**
     * метод обертка
     * для проверки и вывода товаров начиная с 19-го номера
     * сразу после контейнера
     * @return mixed
     */
    public function getDataLayerPageAfter()
    {
        $block = '';
        $more18 = Mage::registry('more18push');
        if ($more18) {
            $block = $more18;
        }
        return $block;
    }

}

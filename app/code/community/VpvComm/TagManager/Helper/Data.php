<?php
/**
 * User: ati
 * Date: 24.12.14
 * Time: 16:09
 * Filename: ${FILE_NAME}
 */
class VpvComm_TagManager_Helper_Data extends Aristos_Base_Helper_Log
{

    use Aristos_Base_Trait_LazyCache;

    public $debugLogFileName = 'tagmanager.log';
    private $storeId = 0;
    private $tgID = 'tg:235226377';
    // размер среза массива
    protected $chunk = 18;
    protected $priceFormatString = '%01.2f';

    /**
     * Использовать ли категорию товара из фида Яндекса (для оптимизации)
     * @var bool
     */
    protected $useMarketCategory = false;

    function __construct()
    {
        parent::__construct();
        $this->setNotifyLevel(Zend_Log::WARN, $this->tgID);
        $this->setNotifyLevel(Zend_Log::ERR, $this->tgID);
        $this->setNotifyLevel(Zend_Log::INFO, $this->tgID);
        $this->storeId = Mage::app()->getStore()->getStoreId();
    }

    /**
     * Получение номера страницы каталога
     * @return string
     */
    public function getCatalogPage()
    {
        $pageNum = '1';
        if ($pager = Mage::app()->getRequest()->getParam('p')) {
            $pageNum = $pager;
        }
        return $pageNum;
    }

    /**
     * получение корректного уровня категории-каталога
     * @param Mage_Catalog_Model_Category $mageObject
     * @return string
     */
    public function getRightLevel(Mage_Catalog_Model_Category $mageObject)
    {
        $level = $mageObject->getLevel();
        return strval($level - 2);
    }

    /**
     * очистка json-строки от лишнего
     * @param $json
     * @return mixed
     */
    public function clearingJson($json)
    {
        try {
            $str = str_replace(
                ['`', "'"],
                ['', ''],
                $json
            );
        } catch (Exception $ex) {
            $str = $json;
            $this->logError('JSON problem', $ex);
        }
        return $str;
    }

    /**
     * обертка для компактного информирования
     * @param $logMessage
     * @param $err
     */
    public function logError($logMessage, $err)
    {
        $this->error(
            $logMessage . ': '
            . $err->getMessage() . ' '
            . $err->getFile() . ' '
            . $err->getLine()
        );
    }

    /**
     * обертка для логирования ошибок
     * @param $logName
     * @param $logMessage
     * @param $err
     * @param int $logLevel
     */
    public function logLog($logName, $logMessage, $err, $logLevel = Zend_Log::ERR)
    {
        $this->setLogName($logName)
            ->log(
                $logMessage . ': '
                . $err->getMessage() . ' '
                . $err->getFile() . ' '
                . $err->getLine(), true, $this->tgID, true, $logLevel
            );
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        return $currencyCode;
    }

    /**
     * Массив для dataLayer для страниц каталога
     * @param $productList
     * @return array
     */
    public function buildCategoryData($productList)
    {
        $data = [];
        if (count($productList) > 0) {
            $data = [
                'event' => 'productlist',
                'ecommerce' => [
                    'currencyCode' => $this->getCurrencyCode(),
                    'impressions' => $productList
                ]
            ];
        }
        return $data;
    }

    /**
     * @param $sku
     * @param $cat
     */
    public function updateSkuCategoryDetails($sku, $cat)
    {
        $session = Mage::getSingleton('core/session');
        $skuCats = $session->getGTMCartSkuCats();
        if (!$skuCats) {
            $skuCats = [];
        }
        $skuCats[$sku] = $cat;
        $session->setGTMCartSkuCats($skuCats);
    }

    /**
     * @param Aristos_Erp_Model_Catalog_Product $product
     * @param float $quantity
     * @param int $position
     * @param $list
     * @param array $notNeed
     * @param string $coupon
     * @return array
     */
    public function improdGenerate($product, $quantity, $position, $list, $notNeed = [], $coupon = '')
    {
        if (!is_null($checkProduct = $product->getProduct())) {
            $product = $checkProduct;
        }
        // цены price и metric1
        $price = $this->priceGood($product);
        $specPrice = $this->getTrueSpecialPrice($product); //$forPrice->getCustomPrice();
        $specialPrice = empty($specPrice) ? $price : $specPrice;
        $specialPrice = sprintf($this->priceFormatString, (float)$specialPrice);
        // доступность dimension18
        $available = $this->getStatusAvailable($product);
        // название категории category
        $categoryName = $this->getFullCategoryName($product);
        $categoryId = $product->getCategoryId();
        $variant = str_replace(['"', "'"], '', $product->getBodyColor());
        $arr = [
            'position' => $position, // iteration
            'id' => $product->getSku(),
            'offer_id' => $product->getId(),
            'name' => $this->nameClear($product->getName()),
            'brand' => $this->getBrand($product),
            'category' => $categoryName,
            'list' => $list, // iteration
            'metric1' => $price,
            'price' => $specialPrice,
            'dimension18' => strval((int)$available),
            'quantity' => $quantity, // iteration
            'variant' => $variant,
            'id_cat' => $categoryId,
            'coupon' => $coupon, // iteration
        ];
        if (count($notNeed) > 0) {
            foreach ($notNeed as $val) {
                unset($arr[$val]);
            }
        }
        return $arr;
    }

    /**
     * Получение полного
     * @param Aristos_Erp_Model_Catalog_Product $product
     * @return string
     */
    public function getFullCategoryName($product)
    {
        $categoryName = $this->lazyWithCache('product_full_category_name_' . $product->getId(), function () use ($product) {
            if ($this->useMarketCategory && ($name = $product->getMarketCategory()))
                return $name;
            return $this->getBaseCategoryNameAsBreadcrumb($product);
        });
        return $categoryName;
    }

    /**
     * Корректное получение бренда
     * @param $product
     * @return mixed
     */
    public function getBrand($product)
    {
        foreach (['manufacturer', 'brend_all'] as $vendorAttr) {
            $vendor = $product->getAttributeText($vendorAttr);
            if (!empty($vendor)) return $vendor;
        }
        return Mage::getStoreConfig('feedyandexmarket/yandexmarket/default_vendor');

    }

    /**
     * формирование типа страницы каталога
     * в зависимости от наличия дочерних страниц
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getCatalogPageType($category)
    {
        $countParentCategories = count($category->getParentCategories());
        $countChildrenCategories = count($category->getChildrenCategories());

        $typeCatalog = 'Catalog';
        if ($countParentCategories == 1 && $countChildrenCategories > 0) {
            $typeCatalog = 'CatalogProductGroup';
        } elseif ($countParentCategories > 1 && $countChildrenCategories > 0) {
            $typeCatalog = 'CatalogCategoryGroup';
        }
        return $typeCatalog;
    }

    /**
     * поля событий работы с формами
     * @return array
     */
    public function formFields()
    {
        $arr = [
            'timingCategory' => 'Forms',
            'timingVar' => false,
            'timingValue' => false,
            'timingLabel' => false,
            'eventContext' => false,
            'eventForm' => false,
            'eventContent' => false,
            'eventError' => false,
            'eventAttemptCount' => false,
        ];
        return $arr;
    }

    /**
     * получение сопутствующих товаров
     * @return array|bool
     */
    public function getAdditionalProducts($prod = false)
    {
        $arr = false;
        $class = 'Aristos_Crosssell_Block_Product';

        if (class_exists($class, false)) {
            $setList = 'AdditionalProducts';
            $cross = new $class();
            $crossProds = $cross->getItemCollection();
            if (count($crossProds) > 0) {
                $arr = $this->iterateImpressions($crossProds, $setList);
            }
        }
        return $arr;
    }

    /**
     * метод возвращает массив
     * просмотренных пользователем товаров
     * @return array|bool
     */
    public function getSeenProducts()
    {
        $arr = false;
        $setList = 'SeenProducts';
        if ($items = $this->getLastViewedProducts()) {
            $arr = $this->iterateImpressions($items, $setList);
        }
        return $arr;
    }

    /**
     * формирование массива товаров начиная с 19-го порядкового номера
     * с разбивкой на блоки по 18 штук
     * @return string
     */
    public function more18pushChunk($impress18_array, $pageCategory)
    {
        $block = "<!-- more18push is empty -->\n";
        $jsonResult = '';
        $i = 19;
        $impressFull = [];
        foreach ($impress18_array as $val) {
            $impress = $this->iterateImpressions($val, $pageCategory, $i);
            $arr = [
                'event' => 'vpvcomm',
                'eventCategory' => 'Non-Interactions',
                'eventAction' => 'show',
                'eventLabel' => 'products',
                'ecommerce' => [
                    'impressions' => $impress
                ],
            ];
            // промежуточное накопление
            array_push($impressFull, $impress);
            $json = json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $data = "\ndataLayer.push({$json});\n";
            $jsonResult .= $data;
            $i += $this->chunk;
        }
        // json-строка для товаров от 19
        array_push(VpvComm_TagManager_Model_AggregatorProducts::$arrProdsAfter, $impressFull);
        return $block;
    }

    /**
     * формирование массива impressions
     * @param $what string какую модель загружаем
     * @param $new_arr bool|array возможность добавить иной массив
     * @return array|bool
     */
    public function getImpressions($what, $setList = false, $new_arr = false)
    {
        $model = Mage::registry($what);
        $arr = [];
        $list = 'UndefinedPage';
        if ($setList) {
            $list = $setList;
        }
        if ($what == 'current_product') {
            $arr = $this->improdGenerate($model, 1, 1, $list, []);
        } elseif ($what == 'current_category') {
            $productListBlock = Mage::app()->getLayout()->getBlock('product_list');
            // проверка на случае если блок будет пустой
            if (is_object($productListBlock)) {
                $collection = $productListBlock->getLoadedProductCollection();
                $items = $collection->getItems();
                $categoryName = $model->getCategory() ? $model->getCategory()->getName() : null;
                $count = count($items);
                if ($count > $this->chunk) {
                    $arr18 = array_chunk($items, $this->chunk);
                    $items18 = array_shift($arr18);
                    $items18more = $arr18;
                    Mage::register('more18push', $this->more18pushChunk($items18more, $categoryName));
                } else {
                    $items18 = $items;
                }
                // первые 18 товаров для dataLayer
                $arr = $this->iterateImpressions($items18, $list);
            }
        }
        if ($new_arr) {
            $arr = $new_arr;
        }
        return $arr;
    }

    /**
     * @param $arr
     * @param string $pageType
     * @return array
     */
    public function getSearchImpressions($arr, $pageType = 'SearchResults')
    {
        $count = count($arr);
        if ($count > $this->chunk) {
            $arr18 = array_chunk($arr, $this->chunk);
            $items18 = array_shift($arr18);
            $items18more = $arr18;
            Mage::register('more18push', $this->more18pushChunk($items18more, $pageType));
        } else {
            $items18 = $arr;
        }
        // первые 18 товаров для dataLayer
        $arr = $this->iterateImpressions($items18, $pageType);
        return $arr;
    }

    /**
     * получение "честного" прайса
     * с учетом дискаунта для данного пользователя
     * @param Aristos_Erp_Model_Catalog_Product $product
     * @return mixed
     */
    public function getTrueSpecialPrice($product)
    {
        return $product->getFinalPrice();
    }

    /**
     * перебор набора товаров
     * и формирова массива impressions
     * @param $arr array массив объектов
     * @param $categoryName string название текущей категории
     * @return array
     */
    public function iterateImpressions($arr, $setList = false, $start = false)
    {
        $arr_impress = [];
        $i = !$start ? 1 : $start;
        $notNeed = ['quantity', 'coupon'];
        if ($setList) {
            $list = $setList;
        } else {
            $list = 'UndefinedPage';
        }
        foreach ($arr as $product) {
            $arr_impress[] = $this->improdGenerate($product, false, $i, $list, $notNeed);
            ++$i;
        }
        return $arr_impress;
    }

    /**
     * проверка статуса доступности товара
     * с доп проверкой на чекаут
     * @param $product
     * @return bool|int
     */
    public function getStatusAvailable($product)
    {
        if (Mage::app()->getRequest()->getRequestedRouteName() == 'checkout') {
            $status = $product->isSaleable();
        } else {
            $productAvailability = Mage::helper('aristos_shipping')->getProductAvailability($product);
            $status = $productAvailability->getPaStatus() == $productAvailability::kStatusInStock ? true : false;
        }
        return $status;
    }

    /**
     * @return mixed
     */
    public function getLastViewedProducts()
    {
        $collection = Mage::getBlockSingleton('reports/product_viewed')->getItemsCollection();
        $items = $collection->getItems();

        return $items;
    }

    /**
     * @return array
     */
    public function productClick()
    {
        $arr = [];
        $cat = Mage::registry('current_category');
        if (!empty($cat)) {
            $arr = [
                'event' => 'vpvcomm',
                'eventCategory' => 'Interactions',
                'eventAction' => 'open',
                'eventLabel' => 'productPage',
                'eventLocation' => $cat->getName(),
                'eventCategoryId' => $cat->getId(),
                'eventPosition' => '+ eventPosition',
                'eventProductId' => '+ eventProductId',
                'ecommerce' => [
                    'click' => [
                        'actionField' => [
                            'list' => $cat->getName(),
                            'products' => []
                        ]
                    ]
                ]
            ];
        }
        return $arr;
    }

    /**
     * @return array
     */
    public function productAddCompare()
    {
        $arr = [];
        $cat = Mage::registry('current_category');
        if (!empty($cat)) {
            $arr = [
                'event' => 'vpvcomm',
                'eventCategory' => 'Conversions',
                'eventAction' => 'add/remove',
                'eventLabel' => 'compare',
                'eventLocation' => $cat->getName(),
                'eventCategoryId' => $cat->getId(),
                'eventPosition' => '+ eventPosition',
                'eventProductId' => '+ eventProductId',
                'eventProductPrice' => '+ eventProductPrice',
                'ecommerce' => [
                    'add' => [
                        'actionField' => [
                            'list' => $cat->getName(),
                            'products' => []
                        ]
                    ]
                ]
            ];
        }
        return $arr;
    }

    /**
     * @param $pageType
     * @return array
     */
    public function allBasket($pageType)
    {
        // сессия для корзины
        $checkoutSess = Mage::getSingleton('checkout/session');
        // товары в корзине
        $items = $checkoutSess->getQuote()->getAllVisibleItems();
        // обработанные товары из корзины
        $basket = $this->getProductsAll($items);
        $arr = [
            'event' => 'vpvcomm',
            'eventCategory' => 'Interactions',
            'eventAction' => 'remove',
            'eventLabel' => 'allCart',
            'ecommerce' => [
                'remove' => [
                    'actionField' => [
                        'list' => $pageType,
                        'products' => $basket
                    ]
                ]
            ]
        ];
        return $arr;
    }

    /**
     * данные для события переключение между табами
     * @return array
     */
    public function tabSwitch()
    {
        $arr = [];
        $prod = Mage::registry('current_product');
        if (!empty($prod)) {
            $prodPrice = $this->priceGood($prod->getPrice());
            $arr = [
                'event' => 'vpvcomm',
                'eventCategory' => 'Interactions',
                'eventAction' => 'switch',
                'eventLabel' => 'tab',
                'eventContent' => '',
                'eventCategoryId' => $prod->getCategory() ? $prod->getCategory()->getId() : null,
                'eventProductId' => $prod->getSku(),
                'eventProductPrice' => $prodPrice,
            ];
        }
        return $arr;
    }

    /**
     * данные продукта для карточки товара
     * @return array
     */
    public function productAddBasket($pageType)
    {
        $arr = [];
        $prod = Mage::registry('current_product');
        if (!empty($prod)) {
            $prodPrice = $this->priceGood($prod->getPrice());
            $category = $prod->getCategory();
            $arr = [
                'event' => 'vpvcomm',
                'eventCategory' => 'Conversions',
                'eventAction' => 'add',
                'eventLabel' => 'cart',
                'eventLocation' => $category ? $category->getName() : null,
                'eventPosition' => '1',
                'eventCategoryId' => $category ? $category->getId() : null,
                'eventProductId' => $prod->getSku(),
                'eventProductPrice' => $prodPrice,
                'ecommerce' => [
                    'add' => [
                        'actionField' => [
                            'list' => 'ProductPage',
                            'products' => $this->improdGenerate($prod, 1, 1, $pageType, [])
                        ]
                    ]
                ]
            ];
        }
        return $arr;
    }

    /**
     * получение полного пути из категорий
     * на основе основной категории
     * из нескольких, к которым отнесен продукт
     * @param Aristos_Erp_Model_Catalog_Product $product
     * @return string
     */
    public function getBaseCategoryNameAsBreadcrumb($product)
    {
        $category = $product->getCategory();
        if (!$category)
            return '';
        $categoryNames = [];
        foreach ($category->getParentCategories() as $parent) {
            $categoryNames[] = $parent->getName();
        }
        $categoryNames = array_unique($categoryNames);
        return implode('/', $categoryNames);
    }

    /**
     * @param $items
     * @return array
     */
    public function getProductsAll($items)
    {
        $arr = [];
        $i = 1;
        foreach ($items as $val) {
            $quantity = intval($val->getQty());
            $arr[] = $this->improdGenerate($val, $quantity, $i, false, ['list']);
            $i++;
        }
        return $arr;
    }

    /**
     * формирование коротких названий товаров
     * @param $name
     * @return string
     */
    public function nameClear($name)
    {
        $result = $name;
        $slash_pos = strpos($name, '/');
        if ($slash_pos !== false) {
            $good = substr($name, 0, $slash_pos + 6);
            $result = substr($good, 0, strrpos($good, ' '));
        }
        $result = str_replace([
            ' - ',
            '"'
        ], [
            '-',
            ''
        ], $result);
        return trim($result);
    }

    /**
     * форматирование цены из БД в корректный формат
     * с двумя нулями после точки
     * @param $price
     * @return string
     */
    public function priceGood($price)
    {
        if (is_object($price)) {
            $price = $price->getPrice() ? $price->getPrice() : false;
        }
        $price = sprintf($this->priceFormatString, (float)$price);
        return $price;
    }

    /**
     * полная информация о продукте по его id
     * @param $id
     * @return array
     */
    public function getProductBuId($id)
    {
        $product = Mage::getModel('catalog/product');
        $prod = $product->load($id);
        $eventLocation = 'eventLocation';
        $eventPosition = 'eventPosition';
        $category = $prod->getCategory();
        $eventCategoryId = $category ? $category->getId() : null;
        $eventProductId = $prod->getSku();
        $eventProductPrice = $this->priceGood($prod->getPrice());
        $specPrice = $this->getTrueSpecialPrice($prod);
        $specialPrice = empty($specPrice) ? $eventProductPrice : $specPrice;
        $categoryName = $category ? $category->getName() : null;
        $list = 'ProductPage';
        $vari = $prod->getBodyColor();
        $variant = empty($vari) ? ' ' : $vari;
        $prodId = $prod->getSku();
        $prodName = $this->nameClear($prod->getName());
        $prodCategory = $categoryName;
        $prodBrand = $prod->getAttributeText('manufacturer');
        $prodPosition = 'prodPosition';
        $prodVariant = $variant;
        $prodDimension18 = '1';
        $prodPrice = $specialPrice;
        $prodMetric1 = $eventProductPrice;
        $prodQuantity = $prod->getQty();
        $arr = [
            'eventLocation' => $eventLocation,
            'eventPosition' => $eventPosition,
            'eventCategoryId' => $eventCategoryId,
            'eventProductId' => $eventProductId,
            'eventProductPrice' => $eventProductPrice,
            'prodId' => $prodId,
            'prodName' => $prodName,
            'prodCategory' => $prodCategory,
            'prodBrand' => $prodBrand,
            'prodPosition' => $prodPosition,
            'prodVariant' => $prodVariant,
            'prodDimension18' => $prodDimension18,
            'prodPrice' => $prodPrice,
            'prodMetric1' => $prodMetric1,
            'prodQuantity' => $prodQuantity,
            'list' => $list
        ];
        return $arr;
    }
}

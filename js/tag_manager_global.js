/**
 * Created by p.vasin on 14.06.17.
 * Global TagManager JS Resource
 */

/**
 * объект для пользовательских событий
 * @type {{on, trigger}}
 */
var customEvents = (function(){
    var eventNode = jQuery({});
    return {
        on: on,
        trigger: trigger
    };
    function on(){
        eventNode.on.apply(eventNode, arguments);
    }
    function trigger(){
        eventNode.trigger.apply(eventNode, arguments);
    }
})();


/**
 * конфигурация
 * @type object
 */
var configTagMan = {
    "selector":{
        "widget":{
            "products":{
                "parent":".products-grid",
                "sku":".sku,.product-sku",
                "shown":".products-grid li,.listing-item",
                "pill":false
            }
        }
    }
};

/**
 * конфиг учета шагов пользователя в checkout
 * @type object
 */
var gtmCheckoutStepsConfig = {
    "1":{
        "trigger":{
            "step":"step_first"
        },
        "selector":{
            "shipping-select-delivery":"courier",
            "shipping-select-pickup":"pickup"
        }
    },
    "2":{
        "trigger":{
            "step":"step_second"
        },
        "selector":{
            "courier":{
                "aristos_du":"office",
                "citycourier_base":"our_courier",
                "deliveryems_deliveryems":"transport_company"
            },
            "pickup":{
                "1":"office",
                "pickup_select":"pickup_point"
            }
        }
    },
    "3":{
        "trigger":{
            "step":"step_third"
        },
        "selector":{
            "cloudpayments":"card_online",
            "cashondelivery":"cash",
            "cardoffline":"card_courier",
            "sbrf":"bank_receipt"
        }
    }
};

/* --- Default selectors for slider objects ---------------------------------------------------------------------- */

var selectorSliderRevolution = '.slider-banner-container .slider-philips';
var selectorSliderBX = '.mb_slider_container';


/* --- Change config for current site ----------------------------------------------------------------------------- */

// расширение дефолтной конфигурации Tag Manager
customEvents.on('config-tag-man', function(evt,data) {
    if (data.hasOwnProperty('addConfig')) {
        jQuery.extend(true,configTagMan,data.addConfig);
    }
});

customEvents.on('slider-config-tag-man', function(evt,data) {
    if (data.hasOwnProperty('revolution')) {
        selectorSliderRevolution = data.revolution;
    }
    if (data.hasOwnProperty('bx')) {
        selectorSliderBX = data.bx;
    }
});

jQuery(document).on('ready ', function () {

    /* --- slider REVOLUTION --------------------------------------------------------------------------------------- */

    /**
     * selectors scheme
     * <div class="slider-banner-container"><div class="slider-philips">
     * <ul class="slides"><li>...</li>
     */

    window.sliderRevolutionHome = jQuery( selectorSliderRevolution );

    /* --- CHECKOUT STEPS ------------------------------------------------------------------------------------------ */

    function generateStepData(step,selector,item){
        var nodeStep = gtmCheckoutStepsConfig[step];
        var trigger = nodeStep.trigger.step;
        var option = false;
        if (typeof item !== 'undefined') {
            option = nodeStep.selector[selector][item];
        } else {
            option = nodeStep.selector[selector];
        }
        var optionExit = typeof option !== 'undefined' ? option : item;
        if (typeof optionExit === 'undefined') {
            optionExit = selector;
        }
        customEvents.trigger(trigger,{
            step: step,
            option: optionExit
        });
    }

// шаг номер = step_first =
    jQuery(window).on('load',function(){
        var legend = jQuery('#delivery-fieldset .legend');
        var selector = legend.find('.active').attr('id');
        if (typeof selector !== 'undefined') {
            generateStepData(1,selector);
        }
    });
    jQuery('#shipping-select-delivery').click(function(){
        generateStepData(1,'shipping-select-delivery');
    });
    jQuery('#shipping-select-pickup').click(function(){
        generateStepData(1,'shipping-select-pickup');
    });

// шаг номер = step_second =
    jQuery('#shipping-methods-row').click(function(){
        var item = jQuery(this).children('ul').find('.active').data('id');
        generateStepData(2,'courier',item);
    });
    jQuery('.pickup-methods').click(function(){
        var item = jQuery(this).find('.active').data('id');
        generateStepData(2,'pickup',item);
    });
    jQuery('#pickup_select').click(function(){
        var item = jQuery(this).attr('id');
        generateStepData(2,'pickup',item);
    });

// шаг номер = step_third =
    jQuery('#payment-methods').click(function(){
        var selector = jQuery(this).children('li.active').data('id');
        generateStepData(3,selector);
    });

});
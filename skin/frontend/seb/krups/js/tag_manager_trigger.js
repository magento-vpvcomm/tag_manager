/**
 * Created by p.vasin
 * Krups
 * Генерация событий
 */


/* --- NEW CONFIG TAG MANAGER for global.js VIA TRIGGER --------------------------------------------------------- */

/**
 * configTagMan.selector.widget.products = parent - sky - shown - pill
 * configTagMan.selector.widget.banners = click.selector - viewport.selector
 */

var newConfigTagMan = {
    "selector":{
        "widget":{
            "products":{
                "key1":"value1"
            },
            "banners":{
                "click":{
                    "key2":"value2"
                },
                "viewport":{
                    "key3":"value3"
                }
            }
        }
    }
};
customEvents.trigger('config-tag-man',{addConfig:newConfigTagMan});

jQuery(document).on('ready pjax:success', function () {

    /* --- BANNER CLICK ------------------------------------------------------------------------------------------ */

    // общая функция триггер для нескольких выборок кликов по баннеру
    function generateTriggerBannerClick(banner_id)
    {
        customEvents.trigger('banner_click',{banner_id: banner_id});
    }

// клик по баннеру-ссылке в слайдере Revolution = !!!
    jQuery('.main-slider-wrap a').click(function(){
        var banner_id = jQuery(this).parents('li').find('.tp-bgimg').attr('src');
        generateTriggerBannerClick(banner_id);
    });

// клик по баннеру-картинке = banner_click = !!!
    jQuery('img[data-banner-id]').click(function(){
        var banner_id = jQuery(this).data('banner-id');
        generateTriggerBannerClick(banner_id);
    });

    /* --- CHECKOUT STEPS ------------------------------------------------------------------------------------------ */

    function generateStepData(step,selector,item){
        var nodeStep = gtmCheckoutStepsConfig[step];
        var trigger = nodeStep.trigger.step;
        var option = false;
        if (typeof item !== 'undefined') {
            option = nodeStep.selector[selector][item];
        } else {
            option = nodeStep.selector[selector];
        }
        customEvents.trigger(trigger,{
            step: step,
            option: option
        });
    }

    // шаг номер = step_first =
    jQuery(window).on('load',function(){
        var legend = jQuery('#delivery-fieldset .legend');
        var selector = legend.find('.active').attr('id');
        if (typeof selector !== 'undefined') {
            generateStepData(1,selector);
        }
    });
    jQuery('#shipping-select-delivery').click(function(){
        generateStepData(1,'shipping-select-delivery');
    });
    jQuery('#shipping-select-pickup').click(function(){
        generateStepData(1,'shipping-select-pickup');
    });

    // шаг номер = step_second =
    jQuery('#shipping-methods-row').click(function(){
        var item = jQuery(this).children('ul').find('.active').data('id');
        generateStepData(2,'courier',item);
    });
    jQuery('.pickup-methods').click(function(){
        var item = jQuery(this).find('.active').data('id');
        generateStepData(2,'pickup',item);
    });
    jQuery('#pickup_select').click(function(){
        var item = jQuery(this).attr('id');
        generateStepData(2,'pickup',item);
    });

    // шаг номер = step_third =
    jQuery('#payment-methods').click(function(){
        var selector = jQuery(this).children('li.active').data('id');
        generateStepData(3,selector);
    });

    /* --- EVENTS -------------------------------------------------------------------------------------------------- */

// клик по товару = product_click = !!!
    jQuery('.products-grid a img, .products-grid h2 a').click(function(){
        var prod_id = jQuery(this).parents('li').find('.sku').text().replace(/\n/g,'').trim();
        var prod_url_text = jQuery(this).attr('href');
        var prod_url_img = jQuery(this).parent().attr('href');
        var prod_url = typeof prod_url_text !== 'undefined' ? prod_url_text : prod_url_img;
        customEvents.trigger('product_click',{prod_id: prod_id, prod_url: prod_url});
    });

// добавить товар в сравнение = product_add_compare = !!!
    jQuery('input:checkbox[name="compare"]').click(function(){
        var prod_id = jQuery(this).parents('li').find('.sku').text().replace(/\n/g,'').trim();
        var is_checked = jQuery(this).prop('checked');
        customEvents.trigger('product_add_compare',{prod_id: prod_id,is_checked:is_checked});
    });


// сортировка товаров = product_sort = !!!
    jQuery('.sort-by li a').click(function() {
        var eventContent = jQuery(this).text().replace(/\n/g,'').trim();
        customEvents.trigger('product_sort',{eventContent: eventContent});
    })


// удаление товара из корзины = product_remove_basket = !!!
    jQuery('.btn-remove').click(function(){
        var prodID = jQuery(this).parents('tr').find('.item-msg:contains("Sku")').text();
        var prodIdClear = prodID.replace(/\n/g,'').replace('* Sku: ','').trim();
        customEvents.trigger('product_remove_basket',{prod_id: prodIdClear});
    });


// очистка корзины = basket_clear = !!!
    jQuery('button[value="empty_cart"]').click(function(){
        customEvents.trigger('basket_clear');
    });


// клик по кнопке "Заказать" = order_confirm = !!!
    jQuery('#submit-order').click(function(){
        customEvents.trigger('order_confirm');
    });


// выбор способа доставки = shipping_select = !!!
    jQuery('*[data-toggle*="shipping-select"]').click(function(){
        var shipMethod = jQuery(this).text().replace(/\n/g, '').trim();
        customEvents.trigger('shipping_select',{shipMethod: shipMethod});
    });


// клик по кнопке "Пересчитать/Обновить" заказ = order_update = !!!
    jQuery('button[value="update_qty"]').click(function(){
        customEvents.trigger('order_update');
    });


// переключение между табами = tab_switch = !!!
    jQuery('#prod_tabs ul.nav li').click(function(){
        var eventContent = jQuery(this, 'a').text().replace(/\n/g, '').trim();
        customEvents.trigger('tab_switch',{eventContent: eventContent});
    });

// добавление товра в корзину = product_add_basket = !!!
    jQuery('.btn-cart, .button-in-stock').click(function(){
        var prod_id_1 = jQuery(this).parents('li').find('.sku').text().replace(/\n/g, '').trim();
        var prod_id_2 = jQuery(this).parents('.product-shop').find('.sku').text().replace(/\n/g, '').trim();
        var prod_id = prod_id_1 !== '' ? prod_id_1 : prod_id_2;
        prod_id = prod_id.replace(/Sku: /g, '').trim();
        customEvents.trigger('product_add_basket',{prod_id:prod_id});
    });

// авторизация пользователя = user_login = !!!
    jQuery('a[href*="customer/account"]').click(function(){
        customEvents.trigger('user_login');
    });

});
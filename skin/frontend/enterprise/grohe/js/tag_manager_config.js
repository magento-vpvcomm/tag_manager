/* --- CONFIG TAG MANAGER -------------------------------------------------------------------------------------- */
var configTagMan = {
    "selector":{
        "widget":{
            "products":{
                "parent":".products-grid",
                "sku":".sku,.product-sku",
                "shown":".products-grid li",
                "pill":false
            }
        }
    }
};
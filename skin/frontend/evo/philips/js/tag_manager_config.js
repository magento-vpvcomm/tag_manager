/* --- CONFIG TAG MANAGER -------------------------------------------------------------------------------------- */

var configTagMan = {
    "selector":{
        "widget":{
            "products":{
                "parent":".masonry-grid-fitrows",
                "sku":".sku",
                "shown":".masonry-grid-fitrows:visible .masonry-grid-item",
                "pill":".masonry-grid-fitrows:visible .masonry-grid-item"
            },
            "banners":{
                "click":{
                    "selector":"img[data-banner-id],ul.slides li a.a-btn.btn-default"
                },
                "viewport":{
                    "selector":"img:visible[data-banner-id]"
                }
            }
        }
    }
};
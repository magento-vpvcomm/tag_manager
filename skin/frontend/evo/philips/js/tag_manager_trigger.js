/**
 * Created by p.vasin
 * Philips
 * Генерация событий
 */

/* --- SLIDERS reconfig --------------------------------------------------------------------------------------- */

// customEvents.trigger('slider-config-tag-man',
//     {
//         revolution: ".slider-banner-container .slider-philips",
//         bx: ".mb_slider_container"
//     }
// );


/* --- NEW CONFIG TAG MANAGER for global.js VIA TRIGGER --------------------------------------------------------- */

/**
 * configTagMan.selector.widget.products = parent - sky - shown - pill
 * configTagMan.selector.widget.banners = click.selector - viewport.selector
 */

var newConfigTagMan = {
    "selector":{
        "widget":{
            "products":{
                "parent":".masonry-grid-fitrows",
                "sku":".sku",
                "shown":".masonry-grid-fitrows:visible .masonry-grid-item",
                "pill":".masonry-grid-fitrows:visible .masonry-grid-item"
            },
            "banners":{
                "click":{
                    "selector":"img[data-banner-id],ul.slides li a.a-btn.btn-default"
                },
                "viewport":{
                    "selector":"img:visible[data-banner-id]"
                }
            }
        }
    }
};

customEvents.trigger('config-tag-man',{addConfig:newConfigTagMan});

// pjax:success

jQuery(document).on('ready', function () {

    /* --- BANNER CLICK ------------------------------------------------------------------------------------------ */

    // общая функция триггер для нескольких выборок кликов по баннеру
    function generateTriggerBannerClick(banner_id)
    {
        customEvents.trigger('banner_click',{banner_id: banner_id});
    }

// клик по баннеру-ссылке в слайдере Revolution
    jQuery('ul.slides li a.a-btn.btn-default').click(function(){
        var banner_id = jQuery(this).parents('li').find('.tp-bgimg').attr('src');
        generateTriggerBannerClick(banner_id);
    });

// клик по баннеру-картинке = banner_click
    jQuery('img[data-banner-id]').click(function(){
        var banner_id = jQuery(this).data('banner-id');
        generateTriggerBannerClick(banner_id);
    });

    /* --- CHECKOUT STEPS ------------------------------------------------------------------------------------------ */

    function generateStepData(step,selector,item){
        var nodeStep = gtmCheckoutStepsConfig[step];
        var trigger = nodeStep.trigger.step;
        var option = false;
        if (typeof item !== 'undefined') {
            option = nodeStep.selector[selector][item];
        } else {
            option = nodeStep.selector[selector];
        }
        customEvents.trigger(trigger,{
            step: step,
            option: option
        });
    }

    // шаг номер = step_first =
    jQuery(window).on('load',function(){
        var legend = jQuery('#delivery-fieldset .legend');
        var selector = legend.find('.active').attr('id');
        generateStepData(1,selector);
    });
    jQuery('#shipping-select-delivery').click(function(){
        generateStepData(1,'shipping-select-delivery');
    });
    jQuery('#shipping-select-pickup').click(function(){
        generateStepData(1,'shipping-select-pickup');
    });

    // шаг номер = step_second =
    jQuery('#shipping-methods-row').click(function(){
        var item = jQuery(this).children('ul').find('.active').data('id');
        generateStepData(2,'courier',item);
    });
    jQuery('.pickup-methods').click(function(){
        var item = jQuery(this).find('.active').data('id');
        generateStepData(2,'pickup',item);
    });
    jQuery('#pickup_select').click(function(){
        var item = jQuery(this).attr('id');
        generateStepData(2,'pickup',item);
    });

    // шаг номер = step_third =
    jQuery('#payment-methods').click(function(){
        var selector = jQuery(this).children('li.active').data('id');
        generateStepData(3,selector);
    });

    /* --- EVENTS -------------------------------------------------------------------------------------------------- */

// клик по товару = product_click = !!!!!!!!!!!!!!
    jQuery('.masonry-grid-item .short-descr h3 a, .masonry-grid-item .overlay-container a').click(function(){
        var prod_id = jQuery(this).parents('.masonry-grid-item').find('.sku').text();
        var prod_url = jQuery(this).attr('href');
        customEvents.trigger('product_click',{prod_id: prod_id, prod_url: prod_url});
    });

// добавить товар в сравнение = product_add_compare = !!!
    jQuery('input:checkbox[name="compare"]').click(function(){
        var prod_id = jQuery(this).parents('.overlay-container').next('.body').find('.sku').text();
        var is_checked = jQuery(this).prop('checked');
        customEvents.trigger('product_add_compare',{prod_id: prod_id,is_checked:is_checked});
    });


// сортировка товаров = product_sort = !!!
    jQuery('#sort-select').on('change', function() {
        var eventContent = jQuery(this).find('option:selected').text();
        customEvents.trigger('product_sort',{eventContent: eventContent});
    })


// удаление товара из корзины = product_remove_basket = !!!
    jQuery('form#cart .remove').click(function(){
        var prod_id = jQuery(this).prevAll('.product').find('.sku').text();
        customEvents.trigger('product_remove_basket',{prod_id: prod_id});
    });


// очистка корзины = basket_clear = !!!
    jQuery('button[value="empty_cart"]').click(function(){
        customEvents.trigger('basket_clear');
    });


// клик по кнопке "Заказать" = order_confirm = !!!
    jQuery('#submit-order').click(function(){
        customEvents.trigger('order_confirm');
    });


// выбор способа доставки = shipping_select = !!!
    jQuery('*[data-toggle*="shipping-select"]').click(function(){
        var shipMethod = jQuery(this).text().replace(/\n/g, '').trim();
        customEvents.trigger('shipping_select',{shipMethod: shipMethod});
    });


// клик по кнопке "Пересчитать/Обновить" заказ = order_update = !!!
    jQuery('button[value="update_qty"]').click(function(){
        customEvents.trigger('order_update');
    });


// переключение между табами = tab_switch = !!!
    jQuery('ul[class*="nav-tabs"] li').click(function(){
        var eventContent = jQuery(this, 'a').text().replace(/\n/g, '').trim();
        customEvents.trigger('tab_switch',{eventContent: eventContent});
    });

// добавление товра в корзину = product_add_basket = !!!
    jQuery('.btn-in-stock, .button-in-cart, .btn-preorder').click(function(){
        var prod_id = jQuery(this).parents('.masonry-grid-item').find('.sku').text();
        customEvents.trigger('product_add_basket',{prod_id:prod_id});
    });

// авторизация пользователя = user_login = !!!
    jQuery('#btn-register, #btnRecoveryPass, .btn-orange, div[class*="ulogin-"]').click(function(){
        customEvents.trigger('user_login');
    });

});

